package trace_validation.buchi_visualiser;

import java.io.*;
import java.util.*;
import trace_validation.buchi_visualiser.interfaces.nba.*;

public class Visualiser {
	public static void main(String args[]) {
		Nba automaton = null;
		
		try {
			String jsonSource = readFile(args[0]);
			automaton = NbaParser.fromJsonString(jsonSource);
		} catch (Exception e) {
			System.err.println(e);
			System.exit(1);
		}
		
		if (automaton == null) {
			System.err.println("Error: Was not able to read automaton");
			System.exit(1);
		}
		
		
		String dotGraph = toDOT(automaton);
		
		String output = new String(args[0]);
		 // Attempt to remove file extension from the end
		output.replaceFirst(".(j|J)(s|S)(o|O)(n|N)$", "");
		output += ".gv";
		
		try {
			writeFile(output, dotGraph);
		} catch (Exception e) {
			System.err.println(e);
			System.exit(1);
		}
		
		System.out.println("Successfully created DOT at " + output);
	}
	
	private static String readFile(String file) throws IOException {
	    BufferedReader reader = new BufferedReader(new FileReader (file));
	    String         line = null;
	    StringBuilder  stringBuilder = new StringBuilder();
	    String         ls = System.getProperty("line.separator");

	    try {
	        while((line = reader.readLine()) != null) {
	            stringBuilder.append(line);
	            stringBuilder.append(ls);
	        }

	        return stringBuilder.toString();
	    } finally {
	        reader.close();
	    }
	}
	
	private static void writeFile(String file, String content) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(file));
	    writer.write(content);
		writer.close();
	}
	
	private static String toDOT(Nba nba) {
		StringBuilder sb = new StringBuilder();
		
		List<State> stateList = new ArrayList<>(nba.getAutomaton().getStates());
		int initialCount = 0;
		
		sb.append("digraph ");
		sb.append(nba.getName().replace(" ", "_"));
		sb.append(" {\n");
		
		// Accepting states
		sb.append("\tnode [shape=doublecircle]");
		for (State state : stateList) {
			if (state.getAccepting()) sb.append(" " + state.getID());
			initialCount++;
		}
		
		// Initial states
		sb.append(";\n\tnode [shape=none label=\"\"]");
		for (int i=0; i<initialCount; i++) {
			sb.append(" i" + i);
		}

		sb.append(";\n\tnode [shape=circle label=\"\\N\"];\n");
		
		// Initial edges
		initialCount = 0;
		for (State state : stateList) {
			if (state.getInitial()) {
				sb.append("\ti" + initialCount + " -> " + state.getID() + ";\n");
				initialCount++;
			}
		}
		
		// Transitions
		for (List<Transition> transition : nba.getAutomaton().getTransitions()) {
			sb.append("\t");
			sb.append(transition.get(0).stringValue);
			sb.append(" -> ");
			sb.append(transition.get(2).stringValue);

			List<String> labels = transition.get(1).stringArrayValue;
			if (labels.size() > 0) {
				sb.append(" [label=\"{");
				for(String label : labels) {
					sb.append(label);
					sb.append(", ");
				}
				sb.delete(sb.length()-2, sb.length());
				sb.append("}\"]");
			} else {
				sb.append(" [label=\"{}\"]");
			}
			sb.append(";\n");
		}
		
		sb.append("}\n");
		
		return sb.toString();
	}
}
