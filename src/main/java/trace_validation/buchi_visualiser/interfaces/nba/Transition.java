package trace_validation.buchi_visualiser.interfaces.nba;

import java.util.*;
import java.io.IOException;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.core.type.*;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.annotation.*;

/**
 * Array of [source ID, {trace event}, destination ID]
 */
@JsonDeserialize(using = Transition.Deserializer.class)
@JsonSerialize(using = Transition.Serializer.class)
public class Transition {
    public List<String> stringArrayValue;
    public String stringValue;

    static class Deserializer extends JsonDeserializer<Transition> {
        @Override
        public Transition deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
            Transition value = new Transition();
            switch (jsonParser.getCurrentToken()) {
            case VALUE_STRING:
                value.stringValue = jsonParser.readValueAs(String.class);
                break;
            case START_ARRAY:
                value.stringArrayValue = jsonParser.readValueAs(new TypeReference<List<String>>() {});
                break;
            default: throw new IOException("Cannot deserialize Transition");
            }
            return value;
        }
    }

    static class Serializer extends JsonSerializer<Transition> {
        @Override
        public void serialize(Transition obj, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            if (obj.stringArrayValue != null) {
                jsonGenerator.writeObject(obj.stringArrayValue);
                return;
            }
            if (obj.stringValue != null) {
                jsonGenerator.writeObject(obj.stringValue);
                return;
            }
            throw new IOException("Transition must not be null");
        }
    }
}