package trace_validation.buchi_visualiser.interfaces.nba;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class State {
    private boolean accepting;
    private String id;
    private boolean initial;

    @JsonProperty("accepting")
    public boolean getAccepting() { return accepting; }
    @JsonProperty("accepting")
    public void setAccepting(boolean value) { this.accepting = value; }

    @JsonProperty("ID")
    public String getID() { return id; }
    @JsonProperty("ID")
    public void setID(String value) { this.id = value; }

    @JsonProperty("initial")
    public boolean getInitial() { return initial; }
    @JsonProperty("initial")
    public void setInitial(boolean value) { this.initial = value; }
}
