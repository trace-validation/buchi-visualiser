package trace_validation.buchi_visualiser.interfaces.nba;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class Automaton {
    private List<State> states;
    private List<List<Transition>> transitions;

    @JsonProperty("states")
    public List<State> getStates() { return states; }
    @JsonProperty("states")
    public void setStates(List<State> value) { this.states = value; }

    @JsonProperty("transitions")
    public List<List<Transition>> getTransitions() { return transitions; }
    @JsonProperty("transitions")
    public void setTransitions(List<List<Transition>> value) { this.transitions = value; }
}
