# Büchi Visualiser

This tool takes the input from the [Requirement Converter](https://gitlab.com/trace-validation/requirement-converter) and converts it to a [DOT](https://en.wikipedia.org/wiki/DOT_(graph_description_language)) graph. This can be used as an easier way to check whether the automaton is as intended.

The results can then be assessed in any DOT viewer (e.g. [WebGraphviz](http://webgraphviz.com/)).

## Requirements
* JDK (developed for `8.0`)
* Apache Maven
* NBA JSON specification

## Usage
1. `git clone https://gitlab.com/trace-validation/buchi-visualiser`
2. `cd buchi-visualiser`
3. `mvn install`
4. `cd target`
5. `java -jar buchi-visualiser-0.0.1-SNAPSHOT-jar-with-dependencies.jar <NBA JSON>`

### Example
This project comes with an example NBA. Note that this does not represent a particular requirement. Run the following command instead of step 5 above.

`java -jar buchi-visualiser-0.0.1-SNAPSHOT-jar-with-dependencies.jar ../src/main/resources/nba_example.json`
